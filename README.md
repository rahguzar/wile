# wile: control iwd managed wifi devices from Emacs

[iwd](https://iwd.wiki.kernel.org/) project provides the executable `iwctl` to control WiFi devices on GNU/Linux. In addition it also provides dbus interfaces for doing the same tasks (`iwctl` uses these under the hood.) This package uses the dbus integration of Emacs to do various WiFi related operations directly from Emacs.

## Managing connection
It provides the following commands:
1. `wile-connect`: connect to a network. Available networks are provided for completion.
2. `wile-scan`: scan to discover available networks.
3. `wile-disconnect`: disconnect from the currently connected network.

## Managing known networks
In addition following commands are available to manage the known networks:
1. `wile-forget`: Forget a known network, know network are provided for completion.
2. `wile-toggle-autoconnect`: Toggle the auto connect property for a known network with completion for known network names.

## Powering WiFi on/off
`wile-toggle-power` can be used to power on/off a WiFi adapter or device.

## Customization
The variable `wile-network-annotation-function` controls the annotations for completion candidates. Two values for it are provided:
1. `wile-default-annotate-network` which uses letters and numbers to display the signal strength of a network and whether a it is open or password protected.
2. `wile-nerd-icon-annotate-network` which uses icons provided by the [nerd-icons](https://github.com/rainstormstudio/nerd-icons.el) to display the same information.
