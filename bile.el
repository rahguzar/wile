;;; bile.el --- Connect to bluetooth devices using Bluez 5 D-BUS API -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Rahguzar
;;
;; Author: Rahguzar <rahguzar@mailbox.org>
;; Maintainer: Rahguzar <rahguzar@mailbox.org>
;; Created: December 26, 2023
;; Modified: December 26, 2023
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://codeberg.org/rahguzar/wile
;; Package-Requires: ((emacs "26.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;; This uses the d-bus api exposed by bluez to allow doing basic bluetooth
;; operations.
;;
;;; Code:
(require 'dbus)

;;; Customization Options
(defvar bile-network-annotation-function 'bile-default-annotate-network
  "A function to annotate the networks.
It should accept four arguments CONNECTEDP, PAIREDP, TRUSTEDP and BLOCKEDP.")

;;; Bluez 5 D-Bus API
(defconst bile-service "org.bluez")

(defun bile--call (path interface method &rest args)
  "Call a bluez serivce METHOD with ARGS on PATH and INTERFACE."
  (apply #'dbus-call-method :system bile-service path interface method args))

(defun bile--name (obj)
  "Get the name (actually alias) of OBJ."
  (caar (alist-get "Alias" obj nil nil #'equal)))

(defun bile--attr (alist attr)
  "Get the value of attribute ATTR from ALIST."
  (caar (alist-get attr alist nil nil #'equal)))

(defun bile--equal (alist attr val)
  "Return non-nil if attribute ATTR is ALIST equals VAL."
  (equal (caar (alist-get attr alist nil nil #'equal)) val))

(defmacro bile--find (item list name-or-path)
  "Find ITEM in LIST by NAME-OR-PATH."
  `(cl-find ,item ,list :test #'equal
    :key ,(if (eq name-or-path 'name)
              '#'bile--name
            '(lambda (elm) (alist-get 'path elm)))))

(defmacro bile--with-device (device &rest body)
  "Execute BODY using DEVICE."
  (declare (indent 1))
  `(progn (when (stringp ,device)
            (setq ,device (bile--find device (bile--filter "Device1") name)))
          ,@body))

(defun bile--get-state ()
  "Get the current state of bluetooth."
  (bile--call "/" "org.freedesktop.DBus.ObjectManager" "GetManagedObjects"))

(defun bile--filter (type &optional state)
  "Return the list of elements of TYPE from STATE."
  (or state (setq state (bile--get-state)))
  (let ((interface (format "%s.%s" bile-service type))
        results)
    (pcase-dolist (`(,path ,elm) state)
      (pcase-dolist (`(,elem-interface ,result) elm)
        (when (equal interface elem-interface)
          (push `(path . ,path) result)
          (push result results))))
    results))

(defun bile--call-adapter-method (method &optional adapter &rest args)
  "Call METHOD with ARGS for ADAPTER if it is powered on."
  (declare (indent 1))
  (let (result poweredp onep)
    (or (listp (caar adapter))
        (progn (setq onep (bile--name adapter))
               (setq adapter (list adapter))))
    (or adapter (setq adapter (bile--filter "Adapter1")))
    (dolist (dev adapter)
      (when (cl-callf or poweredp (bile--attr dev "Powered"))
        (push (apply #'bile--call
                     (alist-get 'path dev)
                     (format "%s.%s" bile-service "Adapter1") method args)
              result)))
    (unless poweredp
      (if onep (error "Adapter %s is not powered on" onep)
        (error "No adapter is powered on")))
    result))

(defun bile--async-connect (device method fmt &rest args)
  "Connect to DEVICE asynchronously by calling METHOD with ARGS.
FMT is used to display a message on success."
  (let* ((count 0)
         error-handler call-handler try-call)
    (setq call-handler
          (lambda ()
            (remove-hook 'dbus-event-error-functions error-handler)
            (message fmt (bile--name device))))
    (setq try-call
          (lambda ()
            (apply #'dbus-call-method-asynchronously
                   :system bile-service (alist-get 'path device)
                   (format "%s.%s" bile-service "Device1")
                   method call-handler args)))
    (setq error-handler
          (lambda (event error)
            (when (eq (nth 9 event) call-handler)
              (cl-incf count)
              (message "Try %s/3: %S. %s" count error
                       (if (> count 2)
                           "Retries exhausted. Aborting"
                         "Trying again"))
              (if (> count 2)
                  (remove-hook 'dbus-event-error-functions error-handler)
                (funcall try-call)))))
    (add-hook 'dbus-event-error-functions error-handler)
    (funcall try-call)))

;;; Completion
(defun bile-default-annotate-network (connected paired trusted blocked)
  "Calculate an annotation using CONNECTED, PAIRED, TRUSTED and BLOCKED."
  (propertize (format "%s%s%s%s  "
                      (if connected "C" "·")
                      (if paired "P" "·")
                      (if trusted "T" "·")
                      (if blocked "B" "·"))
              'face 'completions-annotations))

(defun bile--annotations (devices)
  "Calculate annotations for DEVICES."
  (let (res)
    (dolist (d devices)
      (push (let ((connectedp (bile--attr d "Connected"))
                  (pairedp (bile--attr d "Paired"))
                  (trustedp (bile--attr d "Trusted"))
                  (blockedp (bile--attr d "Blocked")))
              (propertize
               (bile--name d)
               :annotation (funcall bile-network-annotation-function
                                    connectedp pairedp trustedp blockedp)
               :score (+ (if connectedp 8 0) (if pairedp 4 0)
                         (if trustedp 2 0) (if blockedp 0 1))
               :group (bile--attr d "Icon")))
            res))
    (nreverse res)))

(defun bile--affix (cands)
  "Affix CANDS with annotations stored in :annotation property."
  (let (res)
    (dolist (cand cands)
      (push `(,cand ,(get-text-property 0 :annotation cand) "") res))
    (nreverse res)))

(defun bile--group (cand transform)
  "Group function with args CAND and TRANSFORM using :group text property."
  (if transform cand (get-text-property 0 :group cand)))

(defun bile--sort (cands)
  "Sort CANDS based on :score text property."
  (sort cands (lambda (c1 c2) (>= (get-text-property 0 :score c1)
                             (get-text-property 0 :score c2)))))

(defun bile--completion-table (candidates)
  "Completion table completion among CANDIDATES."
  (lambda (str pred action)
    (if (eq action 'metadata)
        '(metadata (affixation-function . bile--affix)
          (display-sort-function . bile--sort)
          (group-function . bile--group))
      (complete-with-action action candidates str pred))))

(defun bile--choose-device (prompt &optional devices)
  "Choose a device from DEVICES using `completing-read' with PROMPT."
  (unless devices (setq devices (bile--filter "Device1")))
  (unless devices (error "No devices are currently available"))
  (let ((table (bile--completion-table (bile--annotations devices))))
    (bile--find (completing-read prompt table nil t) devices name)))

;;; Interactive Commands
;;;###autoload
(defun bile-connect (device &optional undiscovered)
  "Connect to DEVICE. Interactively device names are offered for completion.
UNDISCOVERED non-nil means that this is attempt to connect to possibly unpaired
device in pairing mode without turning on general device discovery."
  (interactive (list (bile--choose-device "Connect to: ") current-prefix-arg))
  (bile--with-device device
    (if undiscovered
        (bile--call-adapter-method "ConnectDevice"
          (bile--find (bile--attr device "Adapter") (bile--filter "Adapter1")
                      path)
          `(:array (:dict-entry "Address"
                                (:variant ,(bile--attr device "Address")))))
      (bile--async-connect
       device "Connect" "Successfully connected to device %s"))))

(defun bile-connect-a2dp (device)
  "Connect to DEVICE using a2dp profile."
  (interactive (list (bile--choose-device "Connect to: ")))
  (bile--with-device device
    (bile--async-connect
     device "ConnectProfile" "Successfully connected to a2dp device %s"
     "0000110b-0000-1000-8000-00805f9b34fb")))

(defun bile-disconnect (device)
  "Disconnect from DEVICE."
  (interactive (list (bile--choose-device "Connect to: ")))
  (bile--with-device device
    (bile--async-connect
     device "Disconnect" "Successfully disconnected from %s")))

(defun bile-pair (device)
  "Pair DEVICE. Interactively device names are offered for completion."
  (interactive (list (bile--choose-device "Connect to: ")))
  (bile--with-device device
    (if (bile--attr device "Paired")
        (message "Device %s is already paired" (bile--name device))
      (let* ((adapter (bile--find (bile--attr device "Adapter")
                                  (bile--filter "Adapter1") path))
             (apath (alist-get 'path adapter))
             (dpath (alist-get 'path device))
             (ainterface (format "%s.%s" bile-service "Adapter1"))
             (set-prop (lambda (prop val)
                         (bile--call
                          apath "org.freedesktop.DBus.Properties" "Set"
                          ainterface prop :variant `(,val)))))
        (unwind-protect
            (progn
              (funcall set-prop "Discoverable" t)
              (funcall set-prop "Pairable" t)
              (bile--call dpath "org.bluez.Device1" "Pair")
              (bile--call dpath "org.freedesktop.DBus.Properties" "Set"
                          (format "%s.%s" bile-service "Device1")
                          "Trusted" :variant '(t))
              (message "Successfully paired with device %s"
                       (bile--name device))
              (bile-stop-discovering))
          (funcall set-prop "Discoverable" nil)
          (funcall set-prop "Pairable" nil))))))

(defun bile-remove-device (device)
  "Remove pairing for DEVICE. Device names are offered for completion."
  (interactive (list (bile--choose-device "Connect to: ")))
  (bile--with-device device
    (bile--call-adapter-method "RemoveDevice"
      (bile--find (bile--attr device "Adapter") (bile--filter "Adapter1") path)
      :object-path (alist-get 'path device))
    (message "Pairing of device %s removed" (bile--name device))))

(defun bile-toggle-trust (device)
  "Toggle the trustedness of DEVICE."
  (interactive (list (bile--choose-device "Connect to: ")))
  (bile--with-device device
    (let ((val (not (bile--attr device "Trusted"))))
      (bile--call (alist-get 'path device) "org.freedesktop.DBus.Properties"
                  "Set" (format "%s.%s" bile-service "Device1") "Trusted"
                  :variant `(,val))
      (message "Device %s is %s trusted" (bile--name device)
               (if val "now" "no longer")))))

(defun bile-start-discovering ()
  "Start device discovery."
  (interactive)
  (bile--call-adapter-method "StartDiscovery")
  (message "Started discovering new bluetooth devices."))

(defun bile-stop-discovering ()
  "Stop device discovery."
  (interactive)
  (bile--call-adapter-method "StopDiscovery")
  (message "Stopped discovering new bluetooth devices."))

(defun bile-toggle-power ()
  "Toggle power of bluetooth adapter."
  (interactive)
  (dolist (a (bile--filter "Adapter1"))
    (let ((val (not (bile--attr a "Powered"))))
      (bile--call
       (alist-get 'path a) "org.freedesktop.DBus.Properties" "Set"
       (format "%s.%s" bile-service "Adapter1") "Powered" :variant `(,val))
      (message "Turned adapter %s %s" (bile--name a) (if val "on" "off")))))

(provide 'bile)
;;; bile.el ends here
