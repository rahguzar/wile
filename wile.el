;;; wile.el --- Emacs interface for iwd -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 rahguzar
;;
;; Maintainer: rahguzar <rahguzar@mailbox.org>
;; Created: November 06, 2023
;; Modified: November 06, 2023
;; Version: 0.0.1
;; Keywords: convenience tools unix wireless wifi
;; Homepage: https://codeberg.org/rahguzar/wile
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; Package to control iwd from Emacs relying on the dbus interface.
;;
;;; Code:
(require 'dbus)

(declare-function nerd-icons-mdicon "nerd-icons")

;;; Customization Options
(defvar wile-network-annotation-function 'wile-default-annotate-network
  "A function to annotate the networks.
It should accept three arguments OPENP, CONNECTEDP and STRENGTH.
OPENP is non-nil if network does not require a password to connect.
CONNECTEDP is non-nil if network is currently connected and STRENGTH
is an integer from 0 to 100 denoting signal strength.")

;;; IWD D-bus API
(defconst wile-service "net.connman.iwd")

(defun wile--call (path interface method &rest args)
  "Call an iwd serivce METHOD with ARGS on PATH and INTERFACE."
  (apply #'dbus-call-method :system wile-service path interface method args))

(defun wile--get-name (network)
  "Get the name of NETWORK."
  (caar (alist-get "Name" network nil nil #'equal)))

(defun wile--get-attr (alist attr)
  "Get the value of attribute ATTR from ALIST."
  (caar (alist-get attr alist nil nil #'equal)))

(defun wile--equal (alist attr val)
  "Return non-nil if attribute ATTR is ALIST equals VAL."
  (equal (caar (alist-get attr alist nil nil #'equal)) val))

(defmacro wile--find (item list name-or-path)
  "Find ITEM in LIST by NAME-OR-PATH."
  `(cl-find ,item ,list :test #'equal
    :key ,(if (eq name-or-path 'name)
              '#'wile--get-name
            '(lambda (elm) (alist-get 'path elm)))))

(defun wile--get-state ()
  "Get the current state of wifi."
  (wile--call "/" "org.freedesktop.DBus.ObjectManager" "GetManagedObjects"))

(defun wile--filter (type &optional state)
  "Return the list of elements of TYPE from STATE."
  (or state (setq state (wile--get-state)))
  (let ((interface (format "%s.%s" wile-service type))
        results)
    (pcase-dolist (`(,path ,elm) state)
      (pcase-dolist (`(,elem-interface ,result) elm)
        (when (equal interface elem-interface)
          (push `(path . ,path) result)
          (push result results))))
    results))

(defun wile--call-station-method (method &optional device setup-fun)
  "Call METHOD for DEVICE if it is in station mode and powered on.
If non-nil call SETUP-FUN with DEVICE as argument before METHOD."
  (let (result stationp poweredp onep)
    (or (listp (caar device)) (progn (setq onep (wile--get-name device))
                                     (setq device (list device))))
    (or device (setq device (wile--filter "Device")))
    (dolist (dev device)
      (when (and (cl-callf or stationp (wile--equal dev "Mode" "station"))
                 (cl-callf or poweredp (wile--get-attr dev "Powered")))
        (when setup-fun (funcall setup-fun dev))
        (push (wile--call (alist-get 'path dev) "net.connman.iwd.Station" method)
              result)))
    (unless poweredp
      (if onep (error "Device %s is not powered on" onep)
        (error "No device is powered on")))
    (unless stationp
      (if onep (error "Device %s is not in station mode" onep)
          "There is no device in station mode"))
    result))

(defun wile--scan-with-callback (&optional device callback)
  "Scan for networks with DEVICE.  After scanning is done call CALLBACK.
CALLBACK should be a function of no arguments."
  (let ((counter 0))
    (wile--call-station-method
     "Scan"
     device
     (lambda (dev)
       (let* ((name (wile--get-name dev))
              (dbus-obj nil)
              (handler (pcase-lambda (_ `((_ (,state))) &rest _)
                         (if state
                             (message "Scan started on device %s" name)
                           (message "Scan finsihed on device %s" name)
                           (dbus-unregister-object dbus-obj)
                           (cl-decf counter)
                           (when (and callback (eq counter 0))
                             (funcall callback))))))
         (cl-incf counter)
         (setq dbus-obj (dbus-register-signal
                         :system wile-service (alist-get 'path dev)
                         "org.freedesktop.DBus.Properties" "PropertiesChanged"
                         handler)))))))

(defun wile--ordered-networks (&optional device)
  "Get the ordered list of networks for DEVICE."
  (let ((networks (apply #'append (wile--call-station-method
                                   "GetOrderedNetworks" device)))
        (networks-alist (wile--filter "Network"))
        result)
    (sort networks (lambda (n1 n2) (< (nth 1 n1) (nth 1 n2))))
    (dolist (n networks)
      (let ((details (wile--find (car n) networks-alist 'path)))
        (push `(signal . ,(nth 1 n)) details)
        (push details result)))
    result))

;;; Completion
(defun wile-default-annotate-network (openp _connectedp strength)
  "Calculate an annotation using OPENP and STRENGTH."
  (propertize (format "%s%s  "
                      (if openp "O" "P")
                      (if strength
                          (format " %s%S" (if (< strength 100) " " "") strength)
                        ""))
              'face 'completions-annotations))

(defun wile-nerd-icon-annotate-network (openp _connectedp strength)
  "Calculate an annotation icon using OPENP and STRENGTH."
  (propertize (concat (nerd-icons-mdicon
                       (format "nf-md-wifi%s%s"
                               (if strength
                                   (format "_strength_%S" (1+ (/ (1- strength) 25)))
                                 "")
                               (if openp "" "_lock")))
                      "  ")
              'face 'completions-annotations))

(defun wile--annotations (networks)
  "Calculate annotations for NETWORKS."
  (let (res)
    (dolist (nw networks)
      (let ((openp (wile--equal nw "Type" "open"))
            (connectedp (wile--get-attr nw "Connected"))
            (knownp (wile--get-attr nw "KnownNetwork"))
            (autop (wile--get-attr nw "AutoConnect"))
            (signal (alist-get 'signal nw)))
        (when signal (setq signal (max 0 (min 100 (* 2 (+ 100 (/ signal 100)))))))
        (push (propertize (wile--get-name nw)
                          :annotation (funcall wile-network-annotation-function
                                               openp connectedp signal)
                          :group (if connectedp "Connected"
                                   (format "%s" (if knownp "Known"
                                                  (if autop "AutoConnect"
                                                    "Other"))))
                          :score (+ (or signal 0) (* 400 (if connectedp 1 0))
                                    (if knownp 200 0) (if autop 1 0)))
              res)))
    (nreverse res)))

(defun wile--affix (cands)
  "Affix CANDS with annotations stored in :annotation property."
  (let (res)
    (dolist (cand cands)
      (push `(,cand ,(get-text-property 0 :annotation cand) "") res))
    (nreverse res)))

(defun wile--group (cand transform)
  "Group function with args CAND and TRANSFORM using :group text property."
  (if transform cand (get-text-property 0 :group cand)))

(defun wile--sort (cands)
  "Sort CANDS based on :score text property."
  (sort cands (lambda (c1 c2) (>= (get-text-property 0 :score c1)
                             (get-text-property 0 :score c2)))))

(defun wile--completion-table (candidates)
  "Completion table completion among CANDIDATES."
  (lambda (str pred action)
    (if (eq action 'metadata)
        '(metadata (affixation-function . wile--affix)
          (display-sort-function . wile--sort)
          (group-function . wile--group))
      (complete-with-action action candidates str pred))))

(defun wile--choose-network (prompt &optional networks)
  "Choose a network from NETWORKS using `completing-read' with PROMPT."
  (unless networks (setq networks (wile--ordered-networks)))
  (unless networks (error "No networks are currently available"))
  (let ((table (wile--completion-table (wile--annotations networks))))
    (wile--find (completing-read prompt table nil t) networks name)))

(defun wile--choose-known-network (prompt)
  "Choose a known network using `completing-read' with PROMPT."
  (if-let ((networks (wile--filter "KnownNetwork")))
      (wile--choose-network prompt networks)
    (error "There are no known networks available")))

(defun wile--annotate-adapter-or-device (adapter-or-device)
  "Return name of ADAPTER-OR-DEVICE annotated with power state and type."
  (propertize (wile--get-name adapter-or-device) :score 0
              :annotation (propertize
                           (if (wile--get-attr adapter-or-device "Powered")
                               "On   "
                             "Off  ")
                           'face 'completions-annotations)
              :group (if (wile--get-attr adapter-or-device "Adapter")
                         "Device"
                       "Adapter")))

(defun wile--choose-adapter-or-device (prompt adapter device)
  "Choose an ADAPTER or DEVICE using `completing-read' with PROMPT."
  (let* ((state (wile--get-state))
         (items (append (when adapter (wile--filter "Adapter" state))
                        (when device (wile--filter "Device" state))))
         (cands (mapcar #'wile--annotate-adapter-or-device items)))
    (wile--find (completing-read prompt (wile--completion-table cands) nil t)
                items name)))

;;; Agent
(defmacro wile--register-agent-method (var method args &rest body)
  "Register BODY as a handler for agent METHOD and push the result to VAR.
ARGS are arguments to the handler."
  (declare (indent 3))
  `(push (dbus-register-method
          :system (dbus-get-unique-name :system) "/org/gnu/Emacs/agent"
          "net.connman.iwd.Agent" ,method
          (lambda ,args
            (condition-case nil
                ,@body
              (quit `(:error ,"net.connman.iwd.Agent.Error.Canceled" "")))))
         ,var))

(defun wile--read-passwd (fmt &rest objects)
  "`read-passwd' with a prompt constructed by passing FMT and OBJECTS to `format'."
  (let ((passwd (read-passwd (apply #'format fmt objects))))
    ;; Clear password on a timer since we have to do it after it has been
    ;; passed to dbus. Use 0 as SECS argument so that clearing happens as
    ;; soon as possible.
    (run-with-timer 0 nil #'clear-string passwd)
    passwd))

(defun wile--register-agent (name)
  "Register agent for connecting to network with NAME."
  (let (objects)
    (wile--register-agent-method objects "Release" () nil)
    (wile--register-agent-method objects "RequestPassphrase" nil
      (wile--read-passwd "Password for %s: " name))
    (wile--register-agent-method objects "RequestPassphrase" (_)
      (wile--read-passwd "Passphrase for %s's key: " name))
    (wile--register-agent-method objects "RequestUserNameAndPassword" (_)
      (let ((user (read-string "Username on %s: " name)))
        `(,user (wile--read-passwd "Password for %s: " user))))
    (wile--register-agent-method objects "RequestUserPassword" (_ user)
      (wile--read-passwd "Password for %s"
                         (if user (format "%s on %s" user name) name)))
    (wile--register-agent-method objects "Cancel" (str)
      (message (format "Attempt to connect to %s cancelled: %s" name str)))
    (wile--call "/net/connman/iwd" "net.connman.iwd.AgentManager"
                "RegisterAgent" :object-path "/org/gnu/Emacs/agent")
    objects))

(defun wile--unregister-agent (agent)
  "Unregister the AGENT."
  (wile--call "/net/connman/iwd" "net.connman.iwd.AgentManager"
              "UnregisterAgent" :object-path "/org/gnu/Emacs/agent")
  (ignore-errors (mapc #'dbus-unregister-object agent)))

(defun wile--maybe-connect-with-agent (network)
  "Connect to NETWORK. Using an agent if needed."
  (let ((name (wile--get-name network))
        (nopasswordp (or (wile--equal network "Type" "open")
                         (alist-get "KnownNetwork" network nil nil #'equal)))
        agent handler error-handler)
    (unless nopasswordp (setq agent (wile--register-agent name)))
    (setq handler (lambda (&rest _)
                    (message "Successfully connected to network %s" name)
                    (remove-hook 'dbus-event-error-functions error-handler)
                    (wile--unregister-agent agent)))
    (setq error-handler
          (lambda (event _error)
            (remove-hook 'dbus-event-error-functions error-handler)
            (wile--unregister-agent agent)
            (when (eq (nth 9 event) handler)
              (message "Failed to connect to network %s.%s" name
                       (if nopasswordp "" " Maybe incorrect password?")))))
    (add-hook 'dbus-event-error-functions error-handler)
    (dbus-call-method-asynchronously
     :system wile-service (alist-get 'path network)
     "net.connman.iwd.Network" "Connect" handler)))

;;; Interactive functions
(defun wile-scan (&optional device)
  "Scan for wifi networks.
By default all devices in station mode are scanned. If non-nil only DEVICE is
scanned. Interactively with a prefix arg prompt for a device name with
completion."
  (interactive (when current-prefix-arg
                 (list (wile--choose-adapter-or-device "Scan: " nil t))))
  (wile--scan-with-callback device))

(defun wile-toggle-power (adapter-or-device)
  "Toggle the power of ADAPTER-OR-DEVICE."
  (interactive (list (wile--choose-adapter-or-device "Toggle: " t t)))
  (let ((interface (format "net.connman.iwd.%s"
                           (if (wile--get-attr adapter-or-device "Adapter")
                               "Device"
                             "Adapter"))))
    (wile--call (alist-get 'path adapter-or-device)
                "org.freedesktop.DBus.Properties" "Set" interface "Powered"
                :variant `(,(not (wile--get-attr adapter-or-device "Powered"))))))

(defun wile-cycle-power ()
  "Turn power off and on for all adapters."
  (interactive)
  (mapc #'wile-toggle-power (wile--filter "Adapter"))
  (run-at-time 4 nil #'mapc #'wile-toggle-power (wile--filter "Adapter")))

;;;###autoload
(defun wile-connect (network &optional scan)
  "Connect to NETWORK.  Interactively network names are offered for completion.
If SCAN is non-nil start a scan and wait for it to finish before offering
completions for network names. Interactively SCAN is the prefix arg."
  (interactive (if current-prefix-arg
                   (list nil t)
                 (list (wile--choose-network "Connect to: "))))
  (if scan
      (wile--scan-with-callback
       nil (lambda () (wile-connect (wile--choose-network "Connect to: "))))
    (when (stringp network)
      (setq network (wile--find network (wile--filter "Network") name)))
    (wile--maybe-connect-with-agent network)))

(defun wile-disconnect (&optional device)
  "Diconnect from all connected networks.
If DEVICE is non-nil only disconnect from that device. Interactively with a
prefix arg prompt for a device name with completion."
  (interactive (when current-prefix-arg
                 (list (wile--choose-adapter-or-device
                        "Disconnect from: " nil t))))
  (wile--call-station-method "Disconnect" device))

(defun wile-forget (network)
  "Forget the known network NETWORK.
Interactively names of known networks are offered for completion."
  (interactive (list (wile--choose-known-network "Forget: ")))
  (when (stringp network)
    (setq network (wile--find network (wile--filter "KnownNetwrok") name)))
  (wile--call (alist-get 'path network) "net.connman.iwd.KnownNetwork" "Forget"))

(defun wile-toggle-auto-connect (network)
  "Toggle auto-connect for known NETWORK."
  (interactive (list (wile--choose-known-network "Toggle auto-connect: ")))
  (when (stringp network)
    (setq network (wile--find network (wile--filter "KnownNetwrok") name)))
  (wile--call (alist-get 'path network) "org.freedesktop.DBus.Properties"
              "Set" "net.connman.iwd.KnownNetwork" "AutoConnect"
              :variant `(,(not (wile--get-attr network "AutoConnect")))))

(provide 'wile)
;;; wile.el ends here
